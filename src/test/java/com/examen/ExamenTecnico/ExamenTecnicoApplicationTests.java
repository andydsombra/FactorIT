package com.examen.ExamenTecnico;

import ExamenTecnico.ExamenTecnicoApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = ExamenTecnicoApplication.class)
class ExamenTecnicoApplicationTests {

	@Test
	void contextLoads() {
	}

}
