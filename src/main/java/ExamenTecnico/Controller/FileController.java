package ExamenTecnico.Controller;

import ExamenTecnico.Entity.Empresa;
import ExamenTecnico.Service.ExcelService;
import ExamenTecnico.Service.FileService;
import ExamenTecnico.Service.XMLService;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.bind.JAXBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/")
public class FileController {

    @Autowired
    private FileService fileService;

    @Autowired
    private XMLService xmlService;

    @Autowired
    private ExcelService excelService;

    String uri = "";
    ArrayList<Empresa> empresa = new ArrayList();

    @PostMapping("/upload")
    private String uploadFile(@RequestParam MultipartFile file, ModelMap model) throws IOException {

        uri="";

        if (file == null || file.isEmpty()) {
            model.put("error", "Por favor suba un archivo");
        }

        try {
            uri = fileService.saveFile(file);
            model.put("ok", "Archivo guardado!");
            System.out.println("Archivo cargado");
        } catch (Exception e) {
            model.put("error", e.getMessage());
        }

        return "redirect:/read";
    }

    @RequestMapping("/read")
    private String readFile(ModelMap model) throws Error, JAXBException {
        empresa.clear();
        try {
            File u = new File(uri);
            empresa = xmlService.read(u );
            model.put("exito", "Desea Vincular el NroContrato en ambas tablas?");
            model.put("Empresa", empresa);
            
        } catch (Error e) {
            model.put("error", e.getMessage());
            model.put("Empresa", empresa);

        }

        return "table.html";
    }
    
    @RequestMapping("/readAnyWay")
    private String readAnyWay(ModelMap model) throws Error, JAXBException {
        try {
            File u = new File(uri);
            
            empresa = xmlService.readAnyWay(u);
            model.put("Empresa", empresa);
            model.put("exito", "Desea Vincular el NroContrato en ambas tablas?");
        } catch (Error e) {
            model.put("error", e.getMessage());
            model.put("Empresa", empresa);

        }


        return "table.html";
    }

    @RequestMapping("/join")
    private String join(ModelMap model) throws Error, JAXBException {
        empresa.clear();
        try {
            File u = new File(uri);
            empresa = xmlService.readJoin(u );
            model.put("Empresa", empresa);

        } catch (Error e) {
            model.put("error", e.getMessage());
            model.put("Empresa", empresa);

        }

        return "table.html";
    }

    @RequestMapping("/excel")
	public ResponseEntity<InputStreamResource> export() throws Exception {
		ByteArrayInputStream stream = excelService.createExcel(empresa);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=Examen-FIT.xlsx");

		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	}

}
