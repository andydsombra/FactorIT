package ExamenTecnico.Entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder ={"movimiento"} )
public class Movimientos {
    private Movimiento movimiento;

    @XmlElement(name="Movimiento")
    public Movimiento getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(Movimiento movimiento) {
        this.movimiento = movimiento;
    }
    
}