
package ExamenTecnico.Entity;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "Empresas")
public class Empresas {
    
    private ArrayList<Empresa> empresa;

    @XmlElement(name="Empresa")
    public ArrayList<Empresa> getEmpresa() {
        return empresa;
    }

    public void setEmpresa(ArrayList<Empresa> empresa) {
        this.empresa = empresa;
    }
 
}