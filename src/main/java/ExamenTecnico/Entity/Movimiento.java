
package ExamenTecnico.Entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlType(propOrder ={"nroContrato","saldoCtaCte","concepto","importe"} )
public class Movimiento {
    
    private Long nroContrato;
    
    private Double saldoCtaCte;
    
    private String concepto;
    
    private Double importe;

    @XmlElement(name= "NroContrato")
    public Long getNroContrato() {
        return nroContrato;
    }

    public void setNroContrato(Long nroContrato) {
        this.nroContrato = nroContrato;
    }

    @XmlElement(name= "SaldoCtaCte")
    public Double getSaldoCtaCte() {
        return saldoCtaCte;
    }

    public void setSaldoCtaCte(Double saldoCtaCte) {
        this.saldoCtaCte = saldoCtaCte;
    }

    @XmlElement(name= "Concepto")
    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    @XmlElement(name= "Importe")
    public Double getImporte() {
        return importe;
    }

    public void setImporte(Double importe) {
        this.importe = importe;
    }
    
    
}
