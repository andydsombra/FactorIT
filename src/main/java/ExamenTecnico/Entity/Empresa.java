
package ExamenTecnico.Entity;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlType(propOrder ={"nroContrato","cuit","denominacion","domicilio", "codigoPostal","productor","movimientos"} )
public class Empresa {
    private Long NroContrato;
    
    private String CUIT;
    
    private String Denominacion;
    
    private String Domicilio;
    
    private Long CodigoPostal;
    
    private String Productor;
    
    private Movimientos movimientos;

    @XmlElement(name= "NroContrato")
    public Long getNroContrato() {
        return NroContrato;
    }

    public void setNroContrato(Long NroContrato) {
        this.NroContrato = NroContrato;
    }

    @XmlElement(name= "CUIT")
    public String getCuit() {
        return CUIT;
    }

    public void setCuit(String CUIT) {
        this.CUIT = CUIT;
    }

    @XmlElement(name= "Denominacion")
    public String getDenominacion() {
        return Denominacion;
    }

    public void setDenominacion(String Denominacion) {
        this.Denominacion = Denominacion;
    }
    
    @XmlElement(name= "Domicilio")
    public String getDomicilio() {
        return Domicilio;
    }

    public void setDomicilio(String Domicilio) {
        this.Domicilio = Domicilio;
    }
    
    @XmlElement(name= "CodigoPostal")
    public Long getCodigoPostal() {
        return CodigoPostal;
    }

    public void setCodigoPostal(Long CodigoPostal) {
        this.CodigoPostal = CodigoPostal;
    }

    @XmlElement(name= "Productor")
    public String getProductor() {
        return Productor;
    }

    public void setProductor(String Productor) {
        this.Productor = Productor;
    }

    @XmlElement(name= "Movimientos")
    public Movimientos getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(Movimientos movimientos) {
        this.movimientos = movimientos;
    }
    
}