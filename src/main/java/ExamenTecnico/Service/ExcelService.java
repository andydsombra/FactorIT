package ExamenTecnico.Service;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import ExamenTecnico.Entity.Empresa;
import java.io.IOException;
import java.util.List;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;


@Service
public class ExcelService {
    public ByteArrayInputStream createExcel(List<Empresa> e ) {
        Workbook book = new XSSFWorkbook();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();


        Sheet sheet = book.createSheet("Empresas");

        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("NroContrato");
        row.createCell(1).setCellValue("CUIT");
        row.createCell(2).setCellValue("Denominación");
        row.createCell(3).setCellValue("Domicilio");
        row.createCell(4).setCellValue("CódigoPostal");
        row.createCell(5).setCellValue("Productor");

        int a=0;
        for (Empresa empresa : e) {
            a=a+1;
            Row row1 = sheet.createRow(a);
            row1.createCell(0).setCellValue(empresa.getNroContrato());
            row1.createCell(1).setCellValue(empresa.getCuit());
            row1.createCell(2).setCellValue(empresa.getDenominacion());
            row1.createCell(3).setCellValue(empresa.getDomicilio());
            row1.createCell(4).setCellValue(empresa.getCodigoPostal());
            row1.createCell(5).setCellValue(empresa.getProductor());
            
        }
        

        Sheet sheet2 = book.createSheet("Movimientos");

        Row row4 = sheet2.createRow(0);
        row4.createCell(0).setCellValue("NroContrato");
        row4.createCell(1).setCellValue("SaldoCtaCte");
        row4.createCell(2).setCellValue("Concepto");
        row4.createCell(3).setCellValue("Importe");

        int b=0;
        for (Empresa empresa : e) {
            b=b+1;
            Row row1 = sheet2.createRow(b);
            if (empresa.getMovimientos().getMovimiento().getNroContrato()== null) {
                row1.createCell(0).setCellValue("null");
            }else{
                row1.createCell(0).setCellValue(empresa.getMovimientos().getMovimiento().getNroContrato());
            }
            row1.createCell(1).setCellValue(empresa.getMovimientos().getMovimiento().getSaldoCtaCte());
            row1.createCell(2).setCellValue(empresa.getMovimientos().getMovimiento().getConcepto());
            row1.createCell(3).setCellValue(empresa.getMovimientos().getMovimiento().getImporte());
            
            
        }

   
		try {
            book.write(stream);
            book.close();
        } catch (IOException e1) {
            
            e1.getMessage();
        }
		
        

        return new ByteArrayInputStream(stream.toByteArray());
    }
}
