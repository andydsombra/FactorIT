package ExamenTecnico.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.springframework.stereotype.Service;
import ExamenTecnico.Entity.Empresa;
import ExamenTecnico.Entity.Empresas;

@Service
public class XMLService {
    public ArrayList<Empresa> read(File file ) throws JAXBException, Error{
        ArrayList<Empresa> empresa= new ArrayList();
        JAXBContext context = JAXBContext.newInstance(Empresas.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        Empresas empresas = (Empresas) unmarshaller.unmarshal(file);

        empresa= empresas.getEmpresa();
        check(empresa);
        
        
        return empresa;
    }

    public ArrayList<Empresa> readJoin(File file ) throws JAXBException, Error{
        ArrayList<Empresa> empresa= new ArrayList();
        JAXBContext context = JAXBContext.newInstance(Empresas.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        Empresas empresas = (Empresas) unmarshaller.unmarshal(file);

        empresa= empresas.getEmpresa();
        for (Empresa e:empresa) {
            e.getMovimientos().getMovimiento().setNroContrato(e.getNroContrato());
        }



        return empresa;
    }
    
    public ArrayList<Empresa> readAnyWay(File file ) throws JAXBException, Error{
        ArrayList<Empresa> empresa= new ArrayList();
        JAXBContext context = JAXBContext.newInstance(Empresas.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        Empresas empresas = (Empresas) unmarshaller.unmarshal(file);

        empresa= empresas.getEmpresa();

        
        return empresa;
    }
    
    public void check(List<Empresa> e){
        for (Empresa empresa : e) {
            if(empresa.getNroContrato()==null){
                throw new Error("El número de contrato de la empresa no se encuentra");
            }
            if(empresa.getCuit()==null){
                throw new Error("El cuit de la empresa no se encuentra");
            }
            if(empresa.getDenominacion()==null){
                throw new Error("El número de contrato de la empresa no se encuentra");
            }
            if(empresa.getDomicilio()==null){
                throw new Error("El domicilio de la empresa no se encuentra");
            }
            if(empresa.getCodigoPostal()==null){
                throw new Error("El código postal de la empresa no se encuentra");
            }
            if(empresa.getProductor()==null){
                throw new Error("El productor de la empresa no se encuentra");
            }
            if(empresa.getMovimientos().getMovimiento().getNroContrato()==null){
                throw new Error("El número de contrato del movimiento no se encuentra");
            }
            if(empresa.getMovimientos().getMovimiento().getSaldoCtaCte()==null){
                throw new Error("El saldo de la CtaCte no se encuentra");
            }
            if(empresa.getMovimientos().getMovimiento().getConcepto()==null){
                throw new Error("El concepto del movimiento no se encuentra");
            }
            if(empresa.getMovimientos().getMovimiento().getImporte()==null){
                throw new Error("El importe del movimiento no se encuentra");
            }
        }
    }
    
    public void completeTag(List<Empresa> e){
        for (Empresa empresa : e) {
            if(empresa.getNroContrato()==null){
                empresa.setNroContrato(Long.MIN_VALUE);
            }
            if(empresa.getCuit()==null){
                empresa.setCuit("-");
            }
            if(empresa.getDenominacion()==null){
                empresa.setDenominacion("-");
            }
            if(empresa.getDomicilio()==null){
                empresa.setDomicilio("-");
            }
            if(empresa.getCodigoPostal()==null){
                empresa.setCodigoPostal(Long.MIN_VALUE);
            }
            if(empresa.getProductor()==null){
                empresa.setProductor("-");
            }
            if(empresa.getMovimientos().getMovimiento().getNroContrato()==null){
                empresa.getMovimientos().getMovimiento().setNroContrato(Long.MIN_VALUE);
            }
            if(empresa.getMovimientos().getMovimiento().getSaldoCtaCte()==null){
                empresa.getMovimientos().getMovimiento().setSaldoCtaCte(Double.MIN_VALUE);
            }
            if(empresa.getMovimientos().getMovimiento().getConcepto()==null){
                empresa.getMovimientos().getMovimiento().setConcepto("-");
            }
            if(empresa.getMovimientos().getMovimiento().getImporte()==null){
                empresa.getMovimientos().getMovimiento().setImporte(Double.MIN_VALUE);
            }
        }
    }
}
