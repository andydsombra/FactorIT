
package ExamenTecnico.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileService {
      
    public String saveFile(MultipartFile file) throws Exception{
        
        String uri="";
        try {
            StringBuilder builder = new StringBuilder();
            builder.append(System.getProperty("user.home"));
            builder.append(File.separator);
            builder.append(file.getOriginalFilename());

            byte[] fileBytes = file.getBytes();
            Path path = Paths.get(builder.toString());
            Files.write(path, fileBytes);
            
            System.out.println(builder.toString());
            uri=builder.toString();

        } catch (IOException e) {
            throw new Exception(e.getMessage());

        }
        return uri;
    }
    
    
    
    

}
